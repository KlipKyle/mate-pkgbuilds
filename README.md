mate-pkgbuilds
================

Arch has out of date MATE packages.  Here are the ones from Manjaro.  It looks
like I might be maintaining these myself for the time being (most likely by
pulling changes from Manjaro occasionally).

I am uploading this mainly for the script, which I put considerable effort
into to make things build in the correct order.

Run make-gtk2.sh to make most of them (everything except the gtk3 variants).
