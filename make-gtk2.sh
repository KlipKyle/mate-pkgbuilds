#!/bin/bash

set -e

maindir="$PWD"
repodir="$PWD/pkgs"
chrootdir="/var/lib/archbuild/extra-x86_64"

[ -d "$repodir" ] || mkdir -p "$repodir"

cd "$maindir/mate-common"
extra-x86_64-build -- -D "$repodir:/pkgs"
cp *.pkg.tar.xz "$repodir/"
cd "$repodir/"
repo-add mate.db.tar.gz *.pkg.tar.xz
cd "$maindir"

sudo cp pacman.conf "$chrootdir/$USER/etc/pacman.conf"

for pkg in mate-desktop libmatekbd libmatemixer libmateweather mate-icon-theme \
		mate-icon-theme-faenza caja marco mate-settings-daemon mate-polkit \
		mate-session-manager mate-menus mate-panel mate-control-center \
		mate-notification-daemon mate-user-guide mate-backgrounds mate-themes pluma \
		mate-terminal mate-utils mate-system-monitor eom engrampa atril \
		caja-extensions mate-user-share mate-media mate-power-manager mate-applets \
		mate-screensaver mate-sensors-applet mozo ; do
	cd "$maindir/$pkg"
	sudo arch-nspawn "$chrootdir/$USER"  --bind-ro="$repodir:/pkgs" pacman -Sy
	makechrootpkg -r "$chrootdir" -D "$repodir:/pkgs"
	cp *.pkg.tar.xz "$repodir/"
	cd "$repodir/"
	repo-add mate.db.tar.gz *.pkg.tar.xz
done
